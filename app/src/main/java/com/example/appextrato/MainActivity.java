package com.example.appextrato;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Button button;
    private TextView login;
    private TextView subtitle;
    private EditText edittextlogin;
    private EditText edittextpassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = findViewById(R.id.button);
        login = findViewById(R.id.login);
        subtitle = findViewById(R.id.subtitle);
        edittextlogin = findViewById(R.id.edittextLogin);
        edittextpassword = findViewById(R.id.edittextpassword);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                verifyLogin();
            }
        });
    }

    public void verifyLogin(){
        if(edittextlogin.getText().toString().equals(edittextpassword.getText().toString())){
            Intent intent = new Intent(MainActivity.this, ListActivity.class);
            startActivity(intent);
        }else{
            //alert dialog
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("Campos inválidos");
            alertDialog.setMessage("Verifique os dados de entrada e tente novamente");
            alertDialog.setPositiveButton("OK", null);
            alertDialog.show();
        }
    }
}
